﻿"use strict";

function generateRandomIP() {
    const min = 0;
    const max = 255;
    const ipArray = [];
    for (let i = 0; i < 4; i++) {
        ipArray.push(Math.floor(Math.random() * (max - min + 1)) + min);
    }
    const randomIP = ipArray.join('.');
    return randomIP;
}

fetch("//api.ipify.org?format=json")
    .then(res => res.json())
    .then(data => {
        localStorage.setItem('ip', `${data.ip}`);
    })
    .catch(err => {
        const storedIP = localStorage.getItem('ip');

        if (storedIP === null || storedIP === undefined) {
            console.log("Không có giá trị IP đã lưu trữ.");
            const randomIP = generateRandomIP();
            console.log(randomIP);
            localStorage.setItem('ip', randomIP);
        } else {
            console.log("Giá trị IP đã được lưu trữ trong localStorage.");
            console.log("IP đã lưu: " + storedIP);
        }
    })

var connection = new signalR.HubConnectionBuilder().withUrl("/notificationHub").build();
var ip = '';
let timerInterval;
connection.start().then(function () {
    connection.invoke("CheckConnected")
}).catch(function (err) {
    return console.error(err.toString());
});

connection.on("Connect", function (status) {
    console.log(status)
    ip = localStorage.getItem('ip');
    console.log(ip)
})

connection.onclose(function (error) {
    console.log('Connection closed due to error: ' + error);
    connection.start().then(function () {
        connection.invoke("CheckConnected")
    }).catch(function (error) {
        console.error(error);
    });

});

function loginadmin() {
    //$('#exampleModal').modal('show');
    window.location.href = "/dang-nhap"
}

function LoginUser() {
    let acc = document.getElementById("accountTeach")
    let pass = document.getElementById("passwordTeach")

    if (acc.value.trim() == "") {
        document.getElementById("accountTeach_err").classList.add("d-block")
    }
    if (pass.value.trim() == "") {
        document.getElementById("passwordTeach_err").classList.add("d-block")
    }
}

function re_call_8() {
    for (let i = 2; i < 9; i++) {
        $(`#otp${i}_2`).prop('disabled', true);
    }
}
function re_call_6() {
    for (let i = 2; i < 7; i++) {
        $(`#otp${i}`).prop('disabled', true);
    }
}